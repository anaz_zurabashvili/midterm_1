package com.example.midterm_1.networking


import com.example.midterm_1.common.ApiEndpoints
import com.example.midterm_1.movie.models.movieDataUrl.MovieDataUrl
import com.example.midterm_1.movie.models.movieItemModel.MovieItemModel
import com.example.midterm_1.movie.models.moviesDataModel.MovieData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @Headers("User-Agent: Retrofit-Sample-App")
    @GET(ApiEndpoints.MOVIES)
    suspend fun getMoviesData(
        @Query("page") page: Int = 1,
        @Query("filters[language]") language: String = "ENG",
        @Query("filters[type]") type: String = "movie",
        @Query("sort") sort: String = "-upload_date",
    ): Response<MovieData>


    @Headers("User-Agent: Retrofit-Sample-App")
    @GET(ApiEndpoints.MOVIES)
    suspend fun getSeriesData(
        @Query("page") page: Int = 1,
        @Query("filters[language]") language: String = "GEO",
        @Query("filters[type]") type: String = "series",
        @Query("sort") sort: String = "-upload_date",
    ): Response<MovieData>

    @Headers("User-Agent: Retrofit-Sample-App")
    @GET(ApiEndpoints.MOVIE_ID)
    suspend fun getSeriesDataId(
        @Path("id") id: Int,
        @Query("filters[language]") language: String = "GEO",
    ): Response<MovieItemModel>


    @Headers("User-Agent: Retrofit-Sample-App")
    @GET(ApiEndpoints.SEARCH)
    suspend fun search(
        @Query("filters[type]") filtersType: String = "movie",
        @Query("keywords") keywords: String,
        @Query("page") page: Int = 1,
        @Query("source") source: String = "adjaranet"
    ): Response<MovieData>

    @Headers("User-Agent: Retrofit-Sample-App")
    @GET(ApiEndpoints.FILE)
    suspend fun getMoviePlayUrl(
        @Path("movie_id") id: Int,
        @Query("source") source: String = "adjaranet"
    ): Response<MovieDataUrl>
}


//    @Headers("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")