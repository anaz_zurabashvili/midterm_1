package com.example.midterm_1.networking

import com.example.midterm_1.common.Resource
import com.example.midterm_1.movie.models.movieDataUrl.MovieDataUrl
import com.example.midterm_1.movie.models.movieItemModel.MovieItemModel
import com.example.midterm_1.movie.models.moviesDataModel.MovieData
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val api: ApiService
) {
    suspend fun getMovies(page: Int, is_movie: Boolean): Resource<MovieData> {
        return try {
            Resource.Loading(null)
            val response = if (is_movie)
                api.getMoviesData(page)
            else
                api.getSeriesData(page)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(data = null, response.message().toString())
            }
        } catch (e: Exception) {
            Resource.Error(data = null, e.toString())
        }
    }

    suspend fun getMovie(id: Int): Resource<MovieItemModel> {
        return try {
            Resource.Loading(null)
            val response = api.getSeriesDataId(id)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(data = null, response.message().toString())
            }
        } catch (e: Exception) {
            Resource.Error(data = null, e.toString())
        }
    }
    suspend fun getMoviePlayUrl(id: Int): Resource<MovieDataUrl> {
        return try {
            Resource.Loading(null)
            val response = api.getMoviePlayUrl(id)
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(data = null, response.message().toString())
            }
        } catch (e: Exception) {
            Resource.Error(data = null, e.toString())
        }
    }


    suspend fun getSearch(keywords: String): Resource<MovieData> {
        return try {
            Resource.Loading(null)
            val response = api.search(keywords = keywords)
            val result = response.body()
            if (response.isSuccessful && result != null)
                Resource.Success(result)
            else
                Resource.Error(message = response.message().toString())
        } catch (e: Exception) {
            Resource.Error(message = e.toString())
        }
    }
}
