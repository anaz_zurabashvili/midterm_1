package com.example.midterm_1.movie.models.moviesDataModel

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieData(
    val data: List<Movie>?,
    val meta: Meta?
)