package com.example.midterm_1.movie.ui.fragments

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.midterm_1.common.Resource
import com.example.midterm_1.databinding.FragmentMovieBinding
import com.example.midterm_1.databinding.LayoutPlayerBinding
import com.example.midterm_1.movie.MoviesViewModel
import com.example.midterm_1.movie.models.moviesDataModel.Movie
import com.example.midterm_1.ui.base.BaseFragment
import com.example.midterm_1.utils.refreshing
import com.example.midterm_1.utils.setImageUrl
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

//@AndroidEntryPoint
class PlayerFragment : BaseFragment<LayoutPlayerBinding>(LayoutPlayerBinding::inflate) {

    private val args: PlayerFragmentArgs by navArgs()

    private lateinit var player: SimpleExoPlayer
    private var playWhenReady = true
    private var currentWindow = 0
    private var playerBackPosition: Long = 0

    override fun init() {
        initPlayer()
    }

    private fun initPlayer() {
        player = SimpleExoPlayer.Builder(requireContext()).build().also {
            binding.exoPlayer.player = it
            val mediaItem =
                MediaItem.fromUri(args.movieUrl)
            it.setMediaItem(mediaItem)
        }
    }

    override fun stop() {
        super.stop()
        player.stop()
    }

}