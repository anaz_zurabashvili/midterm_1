package com.example.midterm_1.movie.ui.fragments


import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.midterm_1.common.Resource
import com.example.midterm_1.databinding.FragmentMovieBinding
import com.example.midterm_1.movie.MoviesViewModel
import com.example.midterm_1.movie.models.moviesDataModel.Movie
import com.example.midterm_1.ui.base.BaseFragment
import com.example.midterm_1.utils.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

//@AndroidEntryPoint
class MovieFragment : BaseFragment<FragmentMovieBinding>(FragmentMovieBinding::inflate) {

    private val viewModel: MoviesViewModel by activityViewModels()
    private val args: MovieFragmentArgs by navArgs()
    private lateinit var auth: FirebaseAuth


    override fun init() {
        auth = FirebaseAuth.getInstance()
        listeners()
        observes()
        viewModel.loadMovie(args.movieId)
        viewModel.loadMoviePlayUrl(args.movieId)
    }

    private fun listeners() {
        binding.swipeStatementFragment.setOnRefreshListener {
            viewModel.loadMovie(args.movieId)
            viewModel.loadMoviePlayUrl(args.movieId)
        }
        if (auth.currentUser != null) {
            binding.btnLove.visible()
        }
    }

    private fun initView(data: Movie) {
        binding.tvTitle.text = data.secondaryName ?: data.primaryName ?: data.originalName
        binding.tvDescription.text = data.plot!!.data!!.description
        binding.tvYear.text = data.year.toString()
        binding.tvDuration.text = String.format(
            getString(STRINGS.duration_format) ,
            (data.duration.toString().toInt() / 3600 * 60 + ((data.duration.toString()
                .toInt() % 3600) / 60)) ,
            (data.duration.toString().toInt() % 60)
        )
        binding.imCover.setImageUrl(data.posters!!.data!!.size240)


        if (data.trailers!!.data!!.isNotEmpty() && data.trailers.data!![0].fileUrl!!.isNotEmpty()) {
            binding.btnTrailer.setOnClickListener {
                openPlayer(data.trailers.data[0].fileUrl!!)
            }
        } else {
            binding.btnTrailer.gone()
        }
        if (auth.currentUser != null) {
            binding.btnLove.visible()
            binding.btnLove.setOnClickListener {
                loveMovie(
                    auth.uid!! ,
                    data.id!! ,
                    (data.secondaryName ?: data.primaryName ?: data.originalName)!! ,
                    data.posters.data!!.size240!!
                )
            }
        }
    }

    private fun observes() {
        observeInfo()
        observeMovie()
    }

    private fun loveMovie(
        userUID: String ,
        movieId: Int ,
        movieTitle: String ,
        movieCover: String
    ) {
        //save movie to user movie list
    }

    private fun observeInfo() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.movieResponse.collectLatest {
                when (it) {
                    is Resource.Success -> {
                        binding.swipeStatementFragment.refreshing(false)
                        initView(it.data!!.data)
                    }
                    else -> binding.swipeStatementFragment.refreshing(true)
                }
            }
        }
    }

    private fun observeMovie() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.movieUrlResponse.collectLatest { data ->
                when (data) {
                    is Resource.Success -> {
                        if (data.data!!.data!!.isNotEmpty()) {
                            binding.btnMovie.visible()
                            binding.btnMovie.setOnClickListener {
//                                for now later I'll fix it
                                openPlayer(data.data.data!![0].files!![0].files!![0].src!!)
                            }
                        } else {
                            binding.btnMovie.gone()
                        }
                    }
                    else -> binding.btnMovie.gone()
                }
            }
        }
    }

    private fun openPlayer(movieUrl: String) {
        findNavController().navigate(
            MovieFragmentDirections.openPlayer(movieUrl)
        )
    }
}