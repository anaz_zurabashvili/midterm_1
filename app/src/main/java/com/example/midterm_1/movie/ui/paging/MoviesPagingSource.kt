package com.example.midterm_1.movie.ui.paging

import android.util.Log.d
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.midterm_1.common.Resource
import com.example.midterm_1.movie.models.moviesDataModel.Movie
import com.example.midterm_1.networking.MovieRepository


class MoviesPagingSource(private val repository: MovieRepository, private val  is_movie:Boolean) :
    PagingSource<Int, Movie>() {

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        val pageNumber = params.key ?: 1
        val response = if (is_movie)
            repository.getMovies(pageNumber, true)
        else
            repository.getMovies(pageNumber, false)
        return when (response) {
            is Resource.Success -> {
                LoadResult.Page(
                    data = response.data!!.data!!,
                    prevKey =  if (pageNumber == 1) null else pageNumber - 1,
                    nextKey =  if (response.data.meta!!.pagination!!.total!! > pageNumber) pageNumber + 1 else null
                )
            }
            is Resource.Error -> {
                LoadResult.Error(Throwable())
            }
            is Resource.Loading-> {
                LoadResult.Error(Throwable())
            }
        }
    }

}