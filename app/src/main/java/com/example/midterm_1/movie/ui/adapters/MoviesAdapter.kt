package com.example.midterm_1.movie.ui.adapters

import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.midterm_1.databinding.ItemMovieBinding
import com.example.midterm_1.movie.models.moviesDataModel.Movie
import com.example.midterm_1.movie.models.moviesDataModel.MovieData
import com.example.midterm_1.utils.setImageUrl


class MoviesAdapter() : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    var clickMovieCallBack: ClickMovieCallBack? = null
    private var movies = mutableListOf<Movie>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: MoviesAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Movie
        fun onBind() {
            model = movies[bindingAdapterPosition]
            binding.tvTitle.text = model.secondaryName ?: model.primaryName ?: model.originalName
            binding.tvPublishDate.text = model.year.toString()
            binding.imCover.setImageUrl(model.posters!!.data!!.size240)
            binding.root.setOnClickListener {
                clickMovieCallBack?.invoke(model.id!!)
            }
        }
    }

    override fun getItemCount() = movies.size

    fun setData(movies: List<Movie>) {
        this.movies = movies.toMutableList()
        notifyDataSetChanged()
    }
}