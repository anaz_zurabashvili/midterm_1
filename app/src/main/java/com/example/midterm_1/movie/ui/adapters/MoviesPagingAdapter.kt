package com.example.midterm_1.movie.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.midterm_1.databinding.ItemMovieBinding
import com.example.midterm_1.movie.models.moviesDataModel.Movie
import com.example.midterm_1.movie.models.moviesDataModel.MovieData
import com.example.midterm_1.utils.setImageUrl

typealias ClickMovieCallBack = (movieId: Int) -> Unit

class MoviesPagingAdapter : PagingDataAdapter<Movie, MoviesPagingAdapter.ViewHolder>(DiffCallback()) {

    var clickMovieCallBack: ClickMovieCallBack? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: MoviesPagingAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind(getItem(position)!!)

    inner class ViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(model: Movie) {
            binding.tvTitle.text = model.secondaryName ?: model.primaryName ?: model.originalName
//            binding.tvDescription.text = model.plot!!.data!!.description
            binding.tvPublishDate.text = model.year.toString()
            binding.imCover.setImageUrl(model.posters!!.data!!.size240)
            binding.root.setOnClickListener {
                clickMovieCallBack?.invoke(model.id!!)
            }
        }
    }


    class DiffCallback : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
            oldItem == newItem
    }
}