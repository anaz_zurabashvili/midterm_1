package com.example.midterm_1.movie.models.moviesDataModel

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Plots(
    val data: List<Data>?
){
    @JsonClass(generateAdapter = true)
    data class Data(
        val description: String?,
        val language: String?
    )
}