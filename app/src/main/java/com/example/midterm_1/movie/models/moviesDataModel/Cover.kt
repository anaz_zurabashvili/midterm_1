package com.example.midterm_1.movie.models.moviesDataModel

data class Cover(
    val large: String?,
    val small: String?
)