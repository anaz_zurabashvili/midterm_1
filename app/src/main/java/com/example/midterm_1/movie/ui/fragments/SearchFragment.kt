package com.example.midterm_1.movie.ui.fragments


import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.midterm_1.common.Resource
import com.example.midterm_1.databinding.FragmentSearchBinding
import com.example.midterm_1.movie.MoviesViewModel
import com.example.midterm_1.movie.ui.adapters.MoviesAdapter
import com.example.midterm_1.ui.base.BaseFragment
import com.example.midterm_1.utils.*
import com.google.firebase.auth.FirebaseAuth

class SearchFragment : BaseFragment<FragmentSearchBinding>(FragmentSearchBinding::inflate) {
    private lateinit var auth: FirebaseAuth
    private lateinit var searchAdapter: MoviesAdapter
    private val viewModel: MoviesViewModel by activityViewModels()

    override fun init() {
        auth = FirebaseAuth.getInstance()
        initRV()
        observes()
        listeners()

    }

    private fun listeners() {
        binding.swipeStatementFragment.setOnRefreshListener {
            viewModel.loadSearch(getString(STRINGS.lotr))
        }

        binding.etSearched.doAfterTextChanged {
            if (!it.isNullOrBlank())
                viewModel.loadSearch(it.toString())
        }
        setAuth()
    }

    private fun setAuth() {
        if (auth.currentUser != null) {
            binding.btnAuth.icon =
                context?.let { ContextCompat.getDrawable(it , DRAWABLES.ic_face) }
            binding.btnAuth.setOnClickListener() {
                openUser()
            }
        } else {
            binding.btnAuth.setOnClickListener() {
                openAuth()
            }
        }
    }

    private fun initRV() {
        binding.rvSearched.apply {
            searchAdapter = MoviesAdapter()
            adapter = searchAdapter
            layoutManager =
                GridLayoutManager(view?.context , 2 , LinearLayoutManager.HORIZONTAL , false)
        }
        searchAdapter.clickMovieCallBack = {
            openMovie(it)
        }
    }

    private fun observes() {
        viewModel.searchResponse.observe(viewLifecycleOwner , {
            when (it) {
                is Resource.Loading -> {
                    binding.swipeStatementFragment.refreshing(true)
                }
                is Resource.Success -> {
                    binding.swipeStatementFragment.refreshing(false)
                    searchAdapter.setData(it.data!!.data!!)
                }
                is Resource.Error -> {
                    binding.swipeStatementFragment.refreshing(true)
                }
            }
        })
    }

    private fun openMovie(movieId: Int) {
        findNavController().navigate(
            SearchFragmentDirections.openMovie(movieId)
        )
    }

    private fun openAuth() {
        findNavController().navigate(
            SearchFragmentDirections.openAuth()
        )
    }

    private fun openUser() {
        findNavController().navigate(
            SearchFragmentDirections.openUser()
        )
    }

}