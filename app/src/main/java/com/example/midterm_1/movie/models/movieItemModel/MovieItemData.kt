package com.example.midterm_1.movie.models.movieItemModel

import com.example.midterm_1.movie.models.moviesDataModel.Movie
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieItemModel (
    val data: Movie
)
