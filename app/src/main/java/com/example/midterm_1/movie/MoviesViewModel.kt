package com.example.midterm_1.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.example.midterm_1.common.Resource
import com.example.midterm_1.movie.models.movieDataUrl.MovieDataUrl
import com.example.midterm_1.movie.models.movieItemModel.MovieItemModel
import com.example.midterm_1.movie.models.moviesDataModel.MovieData
import com.example.midterm_1.movie.ui.paging.MoviesPagingSource
import com.example.midterm_1.networking.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {


    private var _searchResponse = MutableLiveData<Resource<MovieData>>()
    val searchResponse: LiveData<Resource<MovieData>>
        get() = _searchResponse

    private val _movieResponse = MutableStateFlow<Resource<MovieItemModel>>(Resource.Loading(null))
    val movieResponse = _movieResponse.asStateFlow()

    private val _movieUrlResponse = MutableStateFlow<Resource<MovieDataUrl>>(Resource.Loading(null))
    val movieUrlResponse = _movieUrlResponse.asStateFlow()

    fun loadMovie(id: Int) {
        viewModelScope.launch {
            withContext(IO) {
                val resource = movieRepository.getMovie(id)
                _movieResponse.value = resource
            }
        }
    }

    fun loadMoviePlayUrl(id: Int) {
        viewModelScope.launch {
            withContext(IO) {
                val resource = movieRepository.getMoviePlayUrl(id)
                _movieUrlResponse.value = resource
            }
        }
    }

    fun loadSearch(keywords: String) {
        viewModelScope.launch {
            withContext(IO) {
                val resource = movieRepository.getSearch(keywords)
                _searchResponse.postValue(resource)
            }
        }
    }

    fun moviesFlow() =
        Pager(config = PagingConfig(pageSize = 1, maxSize = 20),
            pagingSourceFactory = { MoviesPagingSource(movieRepository, true) })
            .flow.cachedIn(viewModelScope)

    fun seriesFlow() =
        Pager(config = PagingConfig(pageSize = 1, maxSize = 20),
            pagingSourceFactory = { MoviesPagingSource(movieRepository, false) })
            .flow.cachedIn(viewModelScope)

}

