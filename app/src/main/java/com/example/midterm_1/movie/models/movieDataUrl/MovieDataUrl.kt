package com.example.midterm_1.movie.models.movieDataUrl

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieDataUrl(
    val data: List<Data>?
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "air_date")
        val airDate: Any? ,
        val covers: Covers? ,
        val description: String? ,
        val episode: Int? ,
        @Json(name = "episodes_include")
        val episodesInclude: String? ,
        @Json(name = "file_will_be_added_soon")
        val fileWillBeAddedSoon: Boolean? ,
        val files: List<File>? ,
        val poster: String? ,
        val rating: Double? ,
        val title: String? ,
        val upcoming: Boolean?
    ) {
        @JsonClass(generateAdapter = true)
        data class Covers(
            val blurhash: String? ,
            val imageHeight: Int? ,
            val position: String? ,
            val positionPercentage: String? ,
            @Json(name = "1050")
            val x1050: String? ,
            @Json(name = "145")
            val x145: String? ,
            @Json(name = "1920")
            val x1920: String? ,
            @Json(name = "367")
            val x367: String? ,
            @Json(name = "510")
            val x510: String?
        )

        @JsonClass(generateAdapter = true)
        data class File(
            val files: List<FileX>? ,
            val lang: String? ,
            val subtitles: List<Subtitle>?
        ) {
            @JsonClass(generateAdapter = true)
            data class FileX(
                val duration: Int? ,
                val id: Int? ,
                val quality: String? ,
                val src: String? ,
                val thumbnails: List<Thumbnail>?
            ) {
                @JsonClass(generateAdapter = true)
                data class Thumbnail(
                    val columns: Int? ,
                    val duration: Int? ,
                    @Json(name = "end_time")
                    val endTime: Int? ,
                    val height: Int? ,
                    val id: Int? ,
                    val interval: Int? ,
                    @Json(name = "start_time")
                    val startTime: Int? ,
                    val url: String? ,
                    val width: Int?
                )
            }

            @JsonClass(generateAdapter = true)
            data class Subtitle(
                val lang: String? ,
                val url: String?
            )
        }
    }
}