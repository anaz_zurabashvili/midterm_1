package com.example.midterm_1.auth.ui.fragments


import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.midterm_1.databinding.FragmentUserBinding
import com.example.midterm_1.movie.ui.adapters.MoviesAdapter
import com.example.midterm_1.movie.ui.fragments.SearchFragmentDirections
import com.example.midterm_1.ui.base.BaseFragment
import com.example.midterm_1.utils.STRINGS
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class UserFragment : BaseFragment<FragmentUserBinding>(FragmentUserBinding::inflate) {
    private lateinit var auth: FirebaseAuth
    private lateinit var moviesAdapter: MoviesAdapter

    override fun init() {
        auth = FirebaseAuth.getInstance()
        listeners()
        initRV()
    }

    private fun listeners() {
        binding.btnLogout.setOnClickListener {
            logoutUser()
        }
        binding.btnOpenMain.setOnClickListener {
            openMain()
        }
        binding.tvUser.text = auth.currentUser?.email ?: getString(STRINGS.user)
    }

    private fun initRV() {
//        for future
        binding.rvMovies.apply {
            moviesAdapter = MoviesAdapter()
            adapter = moviesAdapter
            layoutManager =
                LinearLayoutManager(view?.context , LinearLayoutManager.HORIZONTAL , false)
        }
        moviesAdapter.clickMovieCallBack = {
            openMovie(it)
        }

    }

    private fun logoutUserNavigate() {
        findNavController().navigate(
            UserFragmentDirections.openMain()
        )
    }

    private fun openMain() {
        findNavController().navigate(
            UserFragmentDirections.openMain()
        )
    }
    private fun openMovie(movieId: Int) {
        findNavController().navigate(
            SearchFragmentDirections.openMovie(movieId)
        )
    }

    private fun logoutUser() {
        if (auth.currentUser != null) {
            auth.signOut()
            logoutUserNavigate()
        }
    }

}