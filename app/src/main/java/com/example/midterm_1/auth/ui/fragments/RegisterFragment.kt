package com.example.midterm_1.auth.ui.fragments

import androidx.navigation.fragment.findNavController
import com.example.midterm_1.databinding.FragmentRegisterBinding
import com.example.midterm_1.ui.base.BaseFragment
import com.example.midterm_1.utils.STRINGS
import com.example.midterm_1.utils.emailValid
import com.example.midterm_1.utils.hideKeyboard
import com.example.midterm_1.utils.showSnackBar
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {
    private lateinit var auth: FirebaseAuth

    override fun init() {
        listeners()
        auth = FirebaseAuth.getInstance()
    }

    private fun listeners() {
        binding.btnRegister.setOnClickListener {
            register()
        }
        binding.btnLogin.setOnClickListener {
            openLogin()
        }
    }

    private fun openLogin() {
        findNavController().navigate(
            RegisterFragmentDirections.openLogin()
        )
    }

    private fun isAllFieldsValid(): Boolean = !(binding.etEmailAddress.text.isNullOrBlank()) &&
            !(binding.etPassword.text.isNullOrBlank())

    private fun makeValidation(): Boolean = isAllFieldsValid()  &&
            binding.etEmailAddress.text.toString().emailValid()

    private fun register() {
        if (makeValidation()) {
            auth.createUserWithEmailAndPassword(binding.etEmailAddress.text.toString() , binding.etPassword.text.toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        binding.root.hideKeyboard()
                        binding.root.showSnackBar("${getString(STRINGS.successful_sign_up)} ${task.result}")
                        openLogin()
                    } else {
                        binding.root.hideKeyboard()
                        binding.root.showSnackBar("${getString(STRINGS.unsuccessful_sign_up)} ${task.exception?.message}")
                    }
                }
        } else {
            binding.root.hideKeyboard()
            binding.root.showSnackBar("${getString(STRINGS.validation_error)}")
        }
    }
}