package com.example.midterm_1.auth.ui.adapters

import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.midterm_1.databinding.ItemMovieBinding
import com.example.midterm_1.databinding.ItemUserMovieBinding
import com.example.midterm_1.movie.models.moviesDataModel.Movie
import com.example.midterm_1.movie.models.moviesDataModel.MovieData
import com.example.midterm_1.movie.ui.adapters.ClickMovieCallBack
import com.example.midterm_1.utils.setImageUrl


class UserMoviesAdapter() : RecyclerView.Adapter<UserMoviesAdapter.ViewHolder>() {

    var clickMovieCallBack: ClickMovieCallBack? = null
    private var movies = mutableListOf<Movie>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            ItemUserMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: UserMoviesAdapter.ViewHolder ,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: ItemUserMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Movie
        fun onBind() {
            model = movies[bindingAdapterPosition]
            binding.tvTitle.text = model.secondaryName ?: model.primaryName ?: model.originalName
            binding.imCover.setImageUrl(model.posters!!.data!!.size240)
            binding.btnStart.setOnClickListener {

                clickMovieCallBack?.invoke(model.id!!)
            }
        }
    }

    override fun getItemCount() = movies.size

    fun setData(movies: List<Movie>) {
        this.movies = movies.toMutableList()
        notifyDataSetChanged()
    }
}