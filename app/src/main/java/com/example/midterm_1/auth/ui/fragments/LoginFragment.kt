package com.example.midterm_1.auth.ui.fragments


import androidx.navigation.fragment.findNavController
import com.example.midterm_1.databinding.FragmentLoginBinding
import com.example.midterm_1.ui.base.BaseFragment
import com.example.midterm_1.utils.STRINGS
import com.example.midterm_1.utils.emailValid
import com.example.midterm_1.utils.hideKeyboard
import com.example.midterm_1.utils.showSnackBar
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {
    private lateinit var auth: FirebaseAuth

    override fun init() {
        listeners()
        auth = FirebaseAuth.getInstance()
    }

    private fun listeners() {
        binding.btnRegister.setOnClickListener {
            openRegister()
        }
        binding.btnLogin.setOnClickListener {
            login()
        }
    }

    private fun openRegister() {
        findNavController().navigate(
            LoginFragmentDirections.openRegister()
        )
    }

    private fun openUser() {
        findNavController().navigate(
            LoginFragmentDirections.openUser()
        )
    }

    private fun isAllFieldsValid(): Boolean = !(binding.etEmailAddress.text.isNullOrBlank() ||
            binding.etPassword.text.isNullOrBlank())

    private fun makeValidation(): Boolean = isAllFieldsValid() &&
            binding.etEmailAddress.text.toString().emailValid()

    private fun login() {
        if (makeValidation()) {
            auth.signInWithEmailAndPassword(
                binding.etEmailAddress.text.toString(),
                binding.etPassword.text.toString()
            )
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        openUser()
                    } else {
                        binding.root.hideKeyboard()
                        binding.root.showSnackBar("${getString(STRINGS.unsuccessful_sign_in)}")
                    }
                }
        } else {
            binding.root.hideKeyboard()
            binding.root.showSnackBar(getString(STRINGS.validation_error))
        }
    }
}