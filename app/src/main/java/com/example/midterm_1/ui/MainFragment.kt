package com.example.midterm_1.ui

import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.midterm_1.databinding.FragmentMainBinding
import com.example.midterm_1.movie.MoviesViewModel
import com.example.midterm_1.movie.ui.adapters.MoviesPagingAdapter
import com.example.midterm_1.movie.ui.paging.MoviesLoadingAdapter
import com.example.midterm_1.ui.base.BaseFragment
import com.example.midterm_1.utils.DRAWABLES
import com.example.midterm_1.utils.gone
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainFragment : BaseFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {
    private lateinit var auth: FirebaseAuth
    private lateinit var moviesAdapter: MoviesPagingAdapter
    private lateinit var seriesAdapter: MoviesPagingAdapter
    private val viewModel: MoviesViewModel by activityViewModels()

    override fun init() {
        auth = FirebaseAuth.getInstance()
        initRV()
        listeners()
        setObserversMovie()
    }

    private fun listeners() {
        binding.btnSearch.setOnClickListener {
            openSearch()
        }
        setAuth()
    }

    private fun setAuth() {
        if (auth.currentUser != null) {
            binding.btnAuth.icon =
                context?.let { ContextCompat.getDrawable(it , DRAWABLES.ic_face) }
            binding.btnAuth.setOnClickListener {
                openUser()
            }
        } else {
            binding.btnAuth.setOnClickListener {
                openAuth()
            }
        }
    }

    private fun setObserversMovie() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.moviesFlow().collectLatest { pagingData ->
                binding.progressBarMovies.gone()
                moviesAdapter.submitData(pagingData)
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.seriesFlow().collectLatest { pagingData ->
                binding.progressBarSeries.gone()
                seriesAdapter.submitData(pagingData)

            }
        }
    }

    private fun initRV() {
        initRVMovies()
        initRVSeries()
    }

    private fun initRVMovies() {
        binding.rvMovies.apply {
            moviesAdapter = MoviesPagingAdapter().apply {
                clickMovieCallBack = {
                    openMovie(it)
                }
            }
            adapter = moviesAdapter.withLoadStateFooter(
                MoviesLoadingAdapter(moviesAdapter)
            )
            layoutManager =
                LinearLayoutManager(view?.context , LinearLayoutManager.HORIZONTAL , false)
        }
    }

    private fun initRVSeries() {
        binding.rvSeries.apply {
            seriesAdapter = MoviesPagingAdapter().apply {
                clickMovieCallBack = {
                    openMovie(it)
                }
            }
            adapter = seriesAdapter.withLoadStateFooter(
                MoviesLoadingAdapter(seriesAdapter)
            )
            layoutManager =
                LinearLayoutManager(view?.context , LinearLayoutManager.HORIZONTAL , false)
        }
    }


    private fun openMovie(movieId: Int) {
        findNavController().navigate(
            MainFragmentDirections.openMovie(movieId)
        )
    }

    private fun openSearch() {
        findNavController().navigate(
            MainFragmentDirections.openSearch()
        )
    }

    private fun openAuth() {
        findNavController().navigate(
            MainFragmentDirections.openAuth()
        )
    }

    private fun openUser() {
        findNavController().navigate(
            MainFragmentDirections.openUser()
        )
    }

}