package com.example.midterm_1.common

object ApiEndpoints {
    const val BASE_URL = "https://api.adjaranet.com/api/v1/"
    const val MOVIES = "movies"
    const val MOVIE_ID = "movies/{id}"
    const val SEARCH = "search"
    const val FILE = "movies/{movie_id}/season-files/0"
}